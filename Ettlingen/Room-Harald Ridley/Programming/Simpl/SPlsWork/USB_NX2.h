namespace USBNX2;
        // class declarations
         class USB_NX2;
         class USB_NX2_Status;
     class USB_NX2 
    {
        // class delegates
        delegate FUNCTION DelegateFnShort ( INTEGER myShort );
        delegate FUNCTION DelegateFnString ( SIMPLSHARPSTRING myString );

        // class events

        // class functions
        FUNCTION Init ();
        FUNCTION PairDevices ();
        FUNCTION RemoveAllPairings ();
        FUNCTION Ping ();
        FUNCTION RemovePairing ();
        FUNCTION GetDevices ();
        FUNCTION SetUpVHub ();
        FUNCTION UnpairVHub ();
        FUNCTION SetVHUBMode ( LONG_INTEGER state );
        FUNCTION EnableUDP ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        STRING siVHubRemoteDevice[][];

        // class properties
        STRING siLocalDevice[];
        STRING siRemoteDevice[];
        STRING siVHubLocalDevice[];
        STRING customIP[];
        INTEGER useCustomIP;
        DelegateProperty DelegateFnShort ErrorMessage;
        DelegateProperty DelegateFnShort ModuleReady;
        DelegateProperty DelegateFnString UDP_Rx;
        DelegateProperty DelegateFnString LocalDevice;
        DelegateProperty DelegateFnString RemoteDevice;
        DelegateProperty DelegateFnShort CurrentVHUBModeDelegate;
    };

     class USB_NX2_Status 
    {
        // class delegates
        delegate FUNCTION DelegateFnShort ( INTEGER myShort );
        delegate FUNCTION DelegateFnStringArr ( SIMPLSHARPSTRING values , INTEGER index );
        delegate FUNCTION DelegateFnShortArr ( INTEGER value , INTEGER index );
        delegate FUNCTION DelegateFnString ( SIMPLSHARPSTRING myString );

        // class events

        // class functions
        FUNCTION Init ();
        FUNCTION SetVHUBMode ();
        FUNCTION GetStatus ();
        FUNCTION PairDevices ();
        FUNCTION RemoveAllPairings ();
        FUNCTION Ping ();
        FUNCTION RemovePairing ();
        FUNCTION GetDevices ();
        FUNCTION SetUpVHub ();
        FUNCTION UnpairVHub ();
        FUNCTION EnableUDP ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        STRING siVHubRemoteDevice[][];

        // class properties
        STRING TargetDevice[];
        LONG_INTEGER VHUBMode;
        DelegateProperty DelegateFnShort OversubscribedDelegate;
        DelegateProperty DelegateFnShort CurrentVHUBModeDelegate;
        DelegateProperty DelegateFnStringArr PairedDevicesDelegate;
        DelegateProperty DelegateFnShortArr PairedDevicesLinkStatusDelegate;
        STRING siLocalDevice[];
        STRING siRemoteDevice[];
        STRING siVHubLocalDevice[];
        STRING customIP[];
        INTEGER useCustomIP;
        DelegateProperty DelegateFnShort ErrorMessage;
        DelegateProperty DelegateFnShort ModuleReady;
        DelegateProperty DelegateFnString UDP_Rx;
        DelegateProperty DelegateFnString LocalDevice;
        DelegateProperty DelegateFnString RemoteDevice;
    };

